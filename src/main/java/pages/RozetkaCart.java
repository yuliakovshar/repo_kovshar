package pages;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RozetkaCart {
    private WebDriver driver;

    public RozetkaCart (WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver=driver;
    }

    @FindBy(xpath = ".//*[@id='drop-block']/h2[text()='Корзина пуста']")
    public WebElement rozetka_title_empty_cart;
}

