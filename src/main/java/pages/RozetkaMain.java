package pages;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RozetkaMain {
    private WebDriver driver;

    public RozetkaMain (WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver=driver;
    }

    @FindBy(xpath = ".//div[@class='logo']/img")
    public WebElement rozetkalogo;

    @FindBy(xpath = ".//*[@class='m-main']/ul//a[contains(text(), 'Apple')]")
    public WebElement rozetka_menu_apple;

    @FindBy(xpath = ".//*[@class='m-main']/ul//a[contains(text(), 'MP3')]")
    public WebElement rozetka_menu_mp3;


    @FindBy(xpath = ".//*[@id='city-chooser']/a")
    public WebElement rozetka_city_choose;

    public RozetkaCitiesPopup clickOnCitiesChooserLink() throws InterruptedException{
        rozetka_city_choose.click();
        return new RozetkaCitiesPopup(driver);
    }

    @FindBy(xpath = ".//div[@name='splash-button']/div[contains(a,'Корзина')]")
    public WebElement rozetka_cart_el;

    public RozetkaCart clickOnCartLink() throws InterruptedException{
        rozetka_cart_el.click();
        return new RozetkaCart(driver);
    }

}
