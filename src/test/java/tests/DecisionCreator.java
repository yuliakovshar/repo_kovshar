package tests;

import org.junit.Rule;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;
import java.util.ArrayList;


public class DecisionCreator {
    static ArrayList<String> testDecision = new ArrayList<String>();
    @Rule
    public TestRule watchman = new TestWatcher() {

        @Override
        public Statement apply(Statement base, Description description) {
            return super.apply(base, description);
        }

        @Override
        protected void succeeded(Description description) {
            System.out.println(description.getDisplayName() + " " + "success!");
            testDecision.add(description.getDisplayName() + " " + "success!");
        }

        @Override
        protected void failed(Throwable e, Description description) {
            System.out.println(description.getDisplayName() + " " + e.getClass().getSimpleName());
            testDecision.add(description.getDisplayName() + " " + e.getClass().getSimpleName());
        }
    };
}