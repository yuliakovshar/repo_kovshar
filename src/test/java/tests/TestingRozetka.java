package tests;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import pages.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static org.junit.Assert.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class TestingRozetka extends DecisionCreator {
    private static WebDriver driver;
    private static pages.RozetkaMain rozetkaMain;
    private static pages.RozetkaCitiesPopup rozetkaCitiesPopup;
    private static pages.RozetkaCart rozetkaCart;
    public static BufferedWriter bw;
    
    
    @BeforeClass
    public static void setUp() throws Exception{
        driver = new FirefoxDriver();
        driver.get("http://rozetka.com.ua/"); //driver.manage().window().maximize();
        driver.manage().window();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        rozetkaMain = new RozetkaMain(driver);
    //---------------------------------------------------------------------------/
        
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
        Date data = new Date();
        File f = new File("C:/temp/ReportFromJenkins/testresults.html");
        bw = new BufferedWriter(new FileWriter(f, true));
        bw.write("<html><body>");
        bw.write("<h3>Test results - " + dateFormat.format(data) + "</h3>");
    }

    @AfterClass
    public static void tearDown() throws Exception{
    	driver.close();
    	for (int i = 0; i < DecisionCreator.testDecision.size(); i++){
        	 bw.write("<br>" + DecisionCreator.testDecision.get(i) + "</br>");
        }
        bw.write("</body></html>");
        bw.close();
        
    }
    
       

    //--------------------------------------------------------------------------------/
    @Test
    public void checkLogoIsDisplayed(){ //test_a
        Assert.assertTrue("Item 'Apple' isn't presented in catalog menu!", rozetkaMain.rozetkalogo.isDisplayed());
    }

    @Test
    public void checkItemAppleNotNull() {  //test_b
        assertNotNull("Item 'Apple' isn't presented in catalog menu!", rozetkaMain.rozetka_menu_apple);
    }

    @Test
    public void checkItemMp3NotNull() { //test_c
        assertNotNull("Item 'MP3' isn't presented in catalog menu!", rozetkaMain.rozetka_menu_mp3);
    }

    @Test
    public void checkKuivKharkovOdessaDisplayed() throws InterruptedException { //test_d
        rozetkaCitiesPopup = rozetkaMain.clickOnCitiesChooserLink();
        Assert.assertTrue("Links on city aren't present!", rozetkaCitiesPopup.rozetka_kuiv_check.isDisplayed());
        Assert.assertTrue("Links on city aren't present!", rozetkaCitiesPopup.rozetka_kharkov_check.isDisplayed());
        Assert.assertTrue("Links on city aren't present!", rozetkaCitiesPopup.rozetka_odessa_check.isDisplayed());
    }

    @Test
    public void checkCartIsEmpty() throws InterruptedException { //test_e
        rozetkaCart = rozetkaMain.clickOnCartLink();
        Assert.assertTrue("The cart aren't empty!", rozetkaCart.rozetka_title_empty_cart.isDisplayed());
    }

}

